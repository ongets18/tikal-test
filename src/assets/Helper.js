export default class Helper {
    /**
     * get country or countries list, with with the most Isolation Agents.
     *
     * @returns {Array}
     */
    getCountryIsolationAgent(dataProvider) {
        //Run time O(n)
        let isolationAgents = this.getIsolationAgents(dataProvider);
        //Run time O(n).
        //TOTAL RUN TIME: O(n) + O(n) = O(n).
        return this.sumIsolationAgentByCountries(isolationAgents);

    }

    /**
     * get only Isolation Agent list.
     *
     * @param dataProvider
     */
    getIsolationAgents(dataProvider) {
        let isolationAgents = {};
        let nonIsolationAgents = {};

        dataProvider.forEach((element) => {
            if (nonIsolationAgents.hasOwnProperty(element.agent)) {
                //if agent in exist in one country, delete from isolation Agents array.
                delete isolationAgents[element.agent];
            } else {
                //set the country in IsolationAgents object by id.
                isolationAgents[element.agent] = element.country;
                //add to exist agent array.
                nonIsolationAgents[element.agent] = true;
            }
        });

        return isolationAgents;
    }

    /**
     * Find and return the country / countries list with the most isolation agents.
     *
     * @param isolationAgents
     * @returns {Array}
     */
    sumIsolationAgentByCountries(isolationAgents) {
        let countriesSumList = [];
        let max = 1;
        let finalResult = [];
        for (let agentId in isolationAgents) {
            if (isolationAgents.hasOwnProperty(agentId)) {
                //fist init -> set 1, for first country.
                if (!countriesSumList[isolationAgents[agentId]]) {
                    countriesSumList[isolationAgents[agentId]] = 1;
                } else {
                    // add 1 if country exist in array.
                    countriesSumList[isolationAgents[agentId]]++;
                }
                if (countriesSumList[isolationAgents[agentId]] > max) {
                    //if has max in countriesSumList array, reset the finalResult, and set the new result.
                    finalResult = [];
                    finalResult.push({
                        country: isolationAgents[agentId],
                        withNumber: countriesSumList[isolationAgents[agentId]]
                    });
                    // max = new max.
                    max = countriesSumList[isolationAgents[agentId]];
                } else if (countriesSumList[isolationAgents[agentId]] === max) {
                    //if max exist, add new country for isolationAgent by countries [condition of some results].
                    finalResult.push({
                        country: isolationAgents[agentId],
                        withNumber: countriesSumList[isolationAgents[agentId]]
                    })
                }
            }


        }
        return finalResult;
    }
}
