export default class DataProvider {
    static getData() {
        return [
            {
                agent: '007', country: 'Brazil',
                address: 'Avenida Vieira Souto 168 Ipanema, Rio de Janeiro',
                date: 'Dec 17, 1995, 9:45:17 PM',
                id:"1"
            },
            {
                agent: '005', country: 'Poland',
                address: 'Rynek Glowny 12, Krakow',
                date: 'Apr 5, 2011, 5:05:12 PM',
                id:"2"
            },
            {
                agent: '007', country: 'Morocco',
                address: '27 Derb Lferrane, Marrakech',
                date: 'Jan 1, 2001, 12:00:00 AM',
                id:"3"
            },
            {
                agent: '005', country: 'Brazil',
                address: 'Rua Roberto Simonsen 122, Sao Paulo',
                date: 'May 5, 1986, 8:40:23 AM',
                id:"4"
            },
            {
                agent: '011', country: 'Poland',
                address: 'swietego Tomasza 35, Krakow',
                date: 'Sep 7, 1997, 7:12:53 PM',
                id:"5"
            },
            {
                agent: '003', country: 'Morocco',
                address: 'Rue Al-Aidi Ali Al-Maaroufi, Casablanca',
                date: 'Aug 29, 2012, 10:17:05 AM',
                id:"6"
            },
            {
                agent: '008', country: 'Brazil',
                address: 'Rua tamoana 418, tefe',
                date: 'Nov 10, 2005, 1:25:13 PM',
                id:"7"
            },
            {
                agent: '013', country: 'Poland',
                address: 'Zlota 9, Lublin',
                date: 'Oct 17, 2002, 10:52:19 AM',
                id:"8"
            },
            {
                agent: '002', country: 'Morocco',
                address: 'Riad Sultan 19, Tangier',
                date: 'Jan 1, 2017, 5:00:00 PM',
                id:"9"
            },
            {
                agent: '009', country: 'Morocco',
                address: 'atlas marina beach, agadir',
                date: 'Dec 1, 2016, 9:21:21 PM',
                id:"10"
            }

        ];
    }

    static getDistanceBetween() {
        return {
            from: "10 Downing st. London",
            toList: [
                {id: "1", distance: 9286.23},
                {id: "2", distance: 1415.14},
                {id: "3", distance: 2301.16},
                {id: "4", distance: 9496.85},
                {id: "5", distance: 1415.57},
                {id: "6", distance: 2081.61},
                {id: "7", distance: 8590.30},
                {id: "8", distance: 1569.36},
                {id: "9", distance: 1804.88},
                {id: "10", distance: 2470.68},
            ]
        }
    }
}
